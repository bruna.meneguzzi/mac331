from geocomp.common.point import Point
from geocomp.common.tree import BST

tree = BST()

print(tree.is_empty())
print(tree.height())

p1 = Point(1, 2, z=None)
p2 = Point(5, 3, z=None)
p3 = Point(3, 2, z=None)

tree.insert((1, p1, 3))
tree.insert((1, p2, 1))

print(tree.getRoot())

tree.print_tree()