from geocomp.common.polygon import Polygon
from geocomp.common.segment import Segment
from geocomp.common.point import Point
from geocomp.common.point import Edge
from .tree import BST

from geocomp.common import control
from geocomp.common.guiprim import *

import numpy as np
import time
import math

# noCone
# recebe dois pontos
# retorna TRUE se a diagonal formada por eles está entre as arestas u.next e u.prev
def noCone(u, v, l):
	prev_u = find_predecessor(l,u)
	suc_u = find_sucessor(l,u)
	if esquerda(prev_u, u, suc_u):
		return esquerda_est(u, v, prev_u) and esquerda_est(v, u, suc_u)
	else:
		return not (esquerda(u, v, suc_u) and esquerda(v, u, prev_u))

# noCone para diagonais
# recebe 2 pontos da diagonal e dois outros pontos
# retorna TRUE se a diagonal estiver no cone formado pelos dois outros pontos
def noConeDv(u, v, prev_u, suc_u):
	if esquerda(suc_u, v , prev_u):
		# se o ponto u é convexo
		return esquerda_est(u, v, prev_u) and esquerda_est(v, u, suc_u)
	else:
		# se o ponto u é côncavo
		return not (esquerda(u, v, suc_u) and esquerda(v, u, prev_u))

# noCone para diagonais
# recebe 2 pontos da diagonal e dois outros pontos
# retorna TRUE se a diagonal estiver no cone formado pelos dois outros pontos
def noConeDx(u, v, prev_u, suc_u):
	if esquerda(prev_u, u , suc_u):
		# se o ponto u é convexo
		return esquerda_est(u, v, prev_u) and esquerda_est(v, u, suc_u)
	else:
		# se o ponto u é côncavo
		return not (esquerda(u, v, suc_u) and esquerda(v, u, prev_u))

# retorna o predecessor de um vértice no polígono original
def find_predecessor(l, point):
	predecessor = 0
	i = 0
	while i < len(l):
		if l[i].x == point.x and l[i].y == point.y:
			predecessor = l[i - 1]
		i = i + 1
	return predecessor

# retorna o sucessor de um vértice no polígono original
def find_sucessor(l, point):
	sucessor = 0
	i = 0
	while i < len(l):
		if l[i].x == point.x and l[i].y == point.y:
			if i != len(l) - 1:
				sucessor = l[i + 1]
			if i == len(l) - 1:
				sucessor = l[0]
		i = i + 1
	return sucessor

# verifica se um vértice é ponta pra baixo
def tip_down(x, Y, n):
	for i in range(0, n):
		if (x.y == Y[i]):
			current_index = i
	if current_index == n - 1:
		predecessor = n - 2
		sucessor = 0

	if current_index == 0:
		predecessor = n - 1
		sucessor = 1

	if (current_index != 0) and (current_index != n - 1):
		predecessor = current_index - 1
		sucessor = current_index + 1

	if (Y[predecessor] > Y[current_index]) and (Y[sucessor] > Y[current_index]):
		return True
	else:
		return False

# primitiva ESQUERDA
# retorna TRUE se o ponto c está à esquerda ou no vetor ab
def esquerda(a, b, c):
	if ((a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x)) >= 0:
		return True
	else:
		return False

# primitiva ESQUERDA+
# retorna TRUE se o ponto c está à esquerda do vetor ab
def esquerda_est(a, b, c):
	if ((a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x)) > 0:
		return True
	else:
		return False

# primitiva ANGULO
# calcula o ângulo entre ab e bc
def angulo(a, b, c):
	dab = (a.x - b.x)**2 + (a.y - b.y)**2
	dbc = (b.x - c.x)**2 + (b.y - c.y)**2
	dac = (a.x - c.x)**2 + (a.y - c.y)**2

	cos_ang = (dab + dbc - dac)/(2*dab*dbc)
	return math.acos(cos_ang)

# algoritmo que ordena os pontos de um polígono y-monótono pela Y-coordenada
def ordena(l):
	if len(l) == 0: 
		return None
	n = len(l)
	X = []
	Y = []
	E = np.array([])
	# cria os vetores X e Y
	for i in range (0, n):
		X.append(l[i].x)
		Y.append(l[i].y)
	# ordena E em função de Y, ordenação indireta
	E = np.argsort(Y)
	# cria e preenche a lista l_y, que é a lista l ordenada pela Y-coordenada
	l_y = []
	for i in reversed (range (0, n)):
		l_y.append(l[E[i]])
	return l_y