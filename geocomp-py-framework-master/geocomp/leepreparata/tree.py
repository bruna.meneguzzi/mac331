from geocomp.common.point import Point
from geocomp.common import control
from geocomp import config

class Node:
    def __init__(self, value):
        #self.key = key
        self.value = value
        self.left_child = None
        self.right_child = None
        self.parent = None
    
class BST:
    def __init__(self):
        self.root = None

    def insert(self, value):
    	if self.root == None:
    		self.root = Node(value)
    	else: 
    		self._insert(value, self.root)

    def _insert(self, value, current_node):
        if self.esquerda((current_node.value[0])[1], (current_node.value[0])[0], value[1]) and \
        self.esquerda((current_node.value[2])[1], (current_node.value[2])[0], value[1]):
            if (current_node.left_child == None):
                current_node.left_child = Node(value)
                current_node.left_child.parent = current_node
            else:
                self._insert(value, current_node.left_child)
        else:
            if (current_node.right_child == None):
                current_node.right_child = Node(value)
                current_node.right_child.parent = current_node
            else:
                self._insert(value, current_node.right_child)

    def getRoot(self):
        return self.root.value

    def is_empty(self):
    	return self.root is None

    def height(self):
        if self.root != None:
            root = self.root
            return self._height(root, 0)
        else:
            return 0

    def _height(self, current_node, current_height):
    	if (current_node == None): 
    		return current_height

    	right_height = self._height(current_node.right_child, current_height + 1)
    	left_height = self._height(current_node.left_child, current_height + 1)
    	return max(left_height, right_height)

    # encontra trapézio na ABBB e devolve o seu nó
    # usado só para auxílio da função delete(value)
    def find(self, value):
        if self.root != None:
            return self._find(value, self.root)
        else:
            return None

    def _find(self, value, current_node):
        if value == current_node.value:
            return current_node
        elif value[1].x < current_node.value[1].x and current_node.left_child != None:
            return self._find(value, current_node.left_child)
        elif value[1].x > current_node.value[1].x and current_node.right_child != None:
            return self._find(value, current_node.right_child)

    # encontra ponto em algum trapézio na ABBB e devolve o trapézio
    def find_point(self, point):
        if self.root != None:
            return self._find_point(point, self.root)
        else:
            return None

    def _find_point(self, point, current_node):
        if self.esquerda((current_node.value[0])[1], (current_node.value[0])[0], point) and \
        self.esquerda((current_node.value[2])[0], (current_node.value[2])[1], point):
        	return current_node
        elif self.esquerda((current_node.value[0])[1], (current_node.value[0])[0], point) and \
        self.esquerda((current_node.value[2])[1], (current_node.value[2])[0], point) and current_node.left_child != None:
            return self._find_point(point, current_node.left_child)
        elif self.esquerda((current_node.value[0])[0], (current_node.value[0])[1], point) and \
        self.esquerda((current_node.value[2])[0], (current_node.value[2])[1], point) and current_node.right_child != None:
            return self._find_point(point, current_node.right_child)

    # retorna True se o trapézio está na ABBB
    def search(self, value):
        if self.root != None:
            return self._search(value, self.root)
        else:
            return False

    def _search(self, value, current_node):
        if (value == current_node.value): 
            return True
        elif value[1].x < current_node.value[1].x and current_node.left_child != None:
            return self._search(value, current_node.left_child)
        elif value[1].x > current_node.value[1].x and current_node.right_child != None:
            return self._search(value, current_node.right_child)
        return False

    # retorna True se o ponto está em algum trapézio na ABBB
    def search_point(self, point):
        if self.root != None:
            return self._search_point(point, self.root)
        else:
            return False

    def _search_point(self, point, current_node):
        if point == (current_node.value[0])[0] or point == (current_node.value[0])[1] or point == current_node.value[1] or point == (current_node.value[2])[0] or point == (current_node.value[2])[1]:
            return True
        elif point.x < current_node.value[1].x and current_node.left_child != None:
            return self._search_point(point, current_node.left_child)
        elif point.x > current_node.value[1].x and current_node.right_child != None:
            return self._search_point(point, current_node.right_child)
        return False    

    def delete(self, value):
        return self.delete_node(self.find_point(value))

    def delete_value(self, value):
        return self.delete_node(self.find(value))

    def delete_node(self, node):

        # retorna o nó com menor valor na árvore
        def min_value_node(n):
            current = n
            while current.left_child != None:
                current = current.left_child
            return current

        # retorna o número de filhos do nó
        def num_children(n):
            num_children = 0
            if n.left_child != None:
                num_children += 1
            if n.right_child != None:
                num_children += 1
            return num_children

        node_parent = node.parent
        node_children = num_children(node)

        # casos
        # Caso 1: nó não tem filhos
        if node_children == 0:

            if node_parent == None:
                self.root = None
                #print("A árvore ficou vazia!")
            elif node_parent.left_child == node:
                node_parent.left_child = None
            else:
                node_parent.right_child = None

        # Caso 2: nó tem apenas um filho
        if node_children == 1:

            # descobre se o filho é left ou right
            if node.left_child != None:
                child = node.left_child
            else:
                child = node.right_child

            if node_parent == None:
                self.root = child
            elif node_parent.left_child == node:
                node_parent.left_child = child
            else:
                node_parent.right_child = child

            # corrige o ponteiro parent
            child.parent = node_parent

        # Caso 3: nó tem dois filhos
        if node_children == 2:

            successor = min_value_node(node.right_child)
            node.value = successor.value
            self.delete_node(successor)

    # retorna True se a ABBB está balanceada e False, caso contrário
    def is_balanced(self):
        return self._is_balanced(self.root) > -1

    def _is_balanced(self, root):
        if root == None:
            return 0

        left_height = self._is_balanced(root.left_child) 
        if left_height == -1:
            return -1

        right_height = self._is_balanced(root.right_child)
        if right_height == -1:
            return -1

        if abs(left_height - right_height) > 1:
            return -1

        return max(left_height, right_height) + 1

    def print_tree(self):
    	if self.root != None:
    		self._print_tree(self.root)

    def _print_tree(self, current_node):
    	if current_node != None:
    		self._print_tree(current_node.left_child)
    		print(current_node.value)
    		self._print_tree(current_node.right_child)

    def esquerda(self, a, b, c):
    	if ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) >= 0:
    		return True
    	else:
    		return False