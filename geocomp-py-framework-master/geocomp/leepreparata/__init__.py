# -*- coding: utf-8 -*-
from . import leepreparata

# cada entrada deve ter:
#  [ 'nome-do-modulo', 'nome-da-funcao', 'nome do algoritmo' ]
children = ( 
	('leepreparata', 'initialize_l', 'Lee e Preparata'),
)

#children = algorithms
#__all__ = [ 'graham', 'gift' ]
#__all__ = [a[0] for a in children]
__all__ = [ 'leepreparata' ]