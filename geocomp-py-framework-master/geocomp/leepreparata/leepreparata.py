from geocomp.common.polygon import Polygon
from geocomp.common.segment import Segment
from .pointedge import MyPoint
from .pointedge import Edge
from .tree import BST
from geocomp.common import control
from geocomp.common.guiprim import *

from .support import *

import numpy as np
import time
import math

#inicialização do leepreparata
def initialize_l(l):
	#construção do polígono
	ret = Polygon(l)
	ret.plot()

	# D_Lee é a lista com os polígonos y-monótonos que a função monotone retorna
	D_Lee = []
	# diag é a lista com as diagonais que o lee e preparata retorna
	diag = []

	dupla = monotone(l)
	D_Lee = dupla[0]
	diag = dupla[1]

	# faz a triangulação em cada polígono y-monótono
	for i in range(0, len(D_Lee)):
		monotone_triangulation(D_Lee[i])

# algoritmo que particiona um polígono em polígonos monótonos
def monotone(l):
	if len (l) == 0: 
		return []

	X = []
	Y = []
	E = np.array([])
	n = len(l)

	# cria os vetores X e Y
	for i in range (0, n):
		X.append(l[i].x)
		Y.append(l[i].y)

	# inicio do processo de criar a DCEL para o polígono inicial	
	edgeList = []
	edgeListTwin = []

	for i in range (0,n):
		e = Edge(l[i], find_sucessor(l, l[i]))
		l[i].incidentEdge = e
		edgeList.append(e)
		e1 = Edge(find_sucessor(l, l[i]), l[i])
		edgeListTwin.append(e1)
		e.twin = e1
		e1.twin = e

	# lidando com pontos extremos
	for i in range (0,n):
		if i == 0:
			e = edgeList[0]
			e.next = edgeList[1]
			e.prev = edgeList[n-1]
			e1 = edgeListTwin[0]
			e1.next = edgeListTwin[n-1]
			e1.prev = edgeListTwin[1]
		if i > 0 and i < n-1:
			e = edgeList[i]
			e.next = edgeList[i+1]
			e.prev = edgeList[i-1]
			e1 = edgeListTwin[i]
			e1.next = edgeListTwin[i-1]
			e1.prev = edgeListTwin[i+1]
		if i == n-1:
			e = edgeList[n-1]
			e.next = edgeList[0]
			e.prev = edgeList[n-2]
			e1 = edgeListTwin[n-1]
			e1.next = edgeListTwin[n-2]
			e1.prev = edgeListTwin[0]

	# término do processo de criar a DCEL para o polígono original

	# ordena E em função de Y, ordenação indireta
	E = np.argsort(Y)
	# cria a lista de trapézios (tuplas)
	trap = BST()
	# cria a lista de diagonais
	D = []
	t = 0

	print(E)

	for i in range(0,n):
		print(Y[E[i]],", ",X[E[i]])

	j = 0
	key = 0
	for i in range(1, len(E)):
		key = E[i]
		j = i-1

		while Y[E[j]] == Y[key] and j >= 0 and X[key] > X[E[j]]:
			E[j+1] = E[j]
			j -= 1
		E[j+1] = key

	for i in range(0,n):
		print(Y[E[i]],", ",X[E[i]])

	# encontra o predecessor e o sucessor
	for k in reversed(range(0, n)):
		# v é o ponto em questão
		v = l[E[k]]
		v.hilight(color="blue")
		# lidando com os pontos extremos
		if (E[k] == 0):
			v_pre = l[n - 1]
			v_suc = l[1]
		if (E[k] == n - 1):
			v_pre = l[n - 2]
			v_suc = l[0]
		if (E[k] != 0) and (E[k] != n - 1):
			v_pre = l[E[k] - 1]
			v_suc = l[E[k] + 1]

		# imprime a linha de varredura
		X_tmp = X
		X_tmp.sort()
		x_min = X_tmp[0]
		x_max = X_tmp[n - 1]
		p1 = MyPoint(x_min - 5, v.y, z=None)
		p2 = MyPoint(x_max + 5, v.y, z=None)
		p1.lineto(p2, color="yellow")

		# verifica que tipo de ponto é v
		if (v_pre.y < v.y < v_suc.y) or (v_pre.y > v.y > v_suc.y) or (v_pre.y > v.y and v.y == v_suc.y and v.x < v_suc.x) or \
		(v_pre.y == v.y and v.y > v_suc.y and v.x > v_suc.x):
			# v tem arestas de lados opostos da linha de varredura
			trata_caso1(trap, v_pre, v, v_suc, Y, n, D, t, l)
		elif (v_pre.y < v.y) or (v_pre.y < v.y and v.y == v_suc.y and v.x < v_suc.x):
			# v tem arestas para baixo da linha de varredura
			trata_caso2(trap, v_pre, v, v_suc, X, n, D, t, l)
		else:
			# v tem arestas para cima da linha de varredura
			trata_caso3(trap, v, Y, n, D, t, l)

		control.sleep()

		# elimina o destaque do ponto e a linha
		v.unhilight()
		p1.remove_lineto(p2)

	# crio uma lista em que os elementos serão os polígonos y-monótonos
	list_of_pol = []
	# crio uma lista que servirá para sabermos quais pontos do polígono ainda não foram percorridos
	list_aux = []

	# inicializo a lista auxiliar com zeros
	for i in range (0,n):
		list_aux.append(0)

	# percorro os pontos do polígono principal e coloco os y-monótonos em list_of_pol
	for i in range (0,n):
		if list_aux[i] == 0:
			# inicio uma lista para o polígono y-monotono corrente
			p1 = []
			# marco que ele já foi acessado
			list_aux[i] = 1
			# adiciono o ponto na lista p1
			p1.append(l[i])
			# encontro a aresta incidente ao ponto
			e = l[i].incidentEdge
			y = e
			# enquando o pŕoximo não for o atual
			while (e.next).point != y.point:
				# o próximo é o novo atual
				e = e.next
				p1.append(e.point)
				# busco qual ponto é esse em l
				for k in range(0,n):
					if l[k] == e.point:
						# marco como acessado
						list_aux[k] = 1
			list_of_pol.append(p1)

	return (list_of_pol,D)

# ponta para o lado | diagonal azul
def trata_caso1(trap, u, v, w, Y, n, D, t, lista):
	if (u.y < w.y):
		t1 = w
		w = u
		u = t1
	i = 0
	j = 0
	x = 0
	k = 0
	l = 0
	joker = None
	joker1 = (trap.find_point(v))
	if (joker1 != None):
		joker = joker1.value
		trap.delete(v)
		i = joker[0][0]
		j = joker[0][1]
		x = joker[1]
		k = joker[2][0]
		l = joker[2][1]
		if (v == j):
			trap.insert(((v, w), v, (k, l)))
		else:
			trap.insert(((i, j), v, (v, w)))
		if (tip_down(x, Y, n)):
			e = add_diagonal(x,v)
			D.append(e)
			x.lineto(v, color="green")

# ponta para cima | diagonal verde
def trata_caso2(trap, u, v, w, X, n, D, t, lista):
	if (esquerda(v, u, w)):
		t1 = w
		w = u
		u = t1
	i = 0
	j = 0
	x = 0
	k = 0
	l = 0
	joker = None
	joker1 = (trap.find_point(v))
	if (joker1 != None):
		joker = joker1.value
		trap.delete(v)
		i = joker[0][0]
		j = joker[0][1]
		x = joker[1]
		k = joker[2][0]
		l = joker[2][1]
		trap.insert(((i, j), v, (v, u)))
		trap.insert(((v, w), v, (k, l)))
		# insere diagonal		
		e = add_diagonal(x,v)
		D.append(e)
		x.lineto(v, color="green")
	else:
		trap.insert(((v, u), v, (v, w)))

# ponta para baixo | diagonal amarela
def trata_caso3(trap, v, Y, n, D, t, lista):
	i = 0
	j = 0
	x = 0
	k = 0
	l = 0
	joker = None
	joker1 = (trap.find_point(v))
	if (joker1 != None):
		joker = joker1.value
		trap.delete(v)
		i = joker[0][0]
		j = joker[0][1]
		x = joker[1]
		k = joker[2][0]
		l = joker[2][1]
		if (tip_down(x, Y, n)):
			# insere diagonal
			e = add_diagonal(x,v)
			D.append(e)
			x.lineto(v, color="green")
	
		# se existir mais um trapézio com esse ponto como suporte inferior
		if (j != v) or (k != v):
			i_1 = 0
			j_1 = 0
			x_1 = 0
			k_1 = 0
			l_1 = 0
			joker = None
			joker1 = (trap.find_point(v))
			if (joker1 != None):
				joker = joker1.value
				trap.delete(v)
				i_1 = joker[0][0]
				j_1 = joker[0][1]
				x_1 = joker[1]
				k_1 = joker[2][0]
				l_1 = joker[2][1]
				if (tip_down(x_1, Y, n)):
					e = add_diagonal(x_1,v)
					D.append(e)
					x_1.lineto(v, color="green")
				if (l == v):
					trap.insert(((i, j), v, (k_1, l_1)))
				else:
					trap.insert(((i_1, j_1), v, (k, l)))

def add_diagonal(x,v):
	# crio as duas arestas em relação à diagonal
	e = Edge(v,x)
	e1 = Edge(x,v)
	# declaro que uma é twin da outra
	e.twin = e1
	e1.twin = e

	# inicializo a e b com qualquer aresta
	a = e
	b = e

	# preciso descobrir as duas arestas vizinhas à nova diagonal no ponto v
	i = v.incidentEdge
	g = (i.twin).point
	while (((i.twin).next).twin).point != g:
		# varro todos as arestas conectadas ao ponto v
		i = (i.twin).next
		p = (((i.twin).next).twin).point
		p1 = (i.twin).point
		if noConeDv(x,v,p,p1):
			# encontro as duas arestas
			a = (i.twin).next
			b = i.twin
			break

	# preciso encontrar as duas arestas vizinhas à nova diagonal no ponto x
	h = x.incidentEdge
	g1 = (h.twin).point
	while (((h.twin).next).twin).point != g1:
		# varro todos as arestas conectadas ao ponto x
		h = (h.twin).next
		p = (((h.twin).next).twin).point
		p1 = (h.twin).point
		if (noConeDx(x,v,p1,p)):
			# arrumo os next/prev das arestas
			a1 = h.twin
			a2 = (h.twin).next
			e.next = a2
			a2.prev = e
			e.prev = b
			b.next = e
			e1.next = a
			a.prev = e1
			a1.next = e1
			e1.prev = a1
			break

	return e
	
# algoritmo de triangulação de polígonos y-monótonos
def monotone_triangulation(l):
	#vetor com os vértices ordenados pela y-coordenada
	l_y = ordena(l)

	# n é o tamanho do vetor l_y
	n = len(l_y)

	# cria pilha
	S = []

	# coloca l_y[0] e l_y[1] em S
	S.append(l_y[0])
	S.append(l_y[1])

	# lista de diagonais
	D = []
	t = 0

	# listas com os elementos predecessores e sucessores de um elemento
	lista_pre = []
	lista_suc = []

	# constroi listas com curvas poligonais esquerda (l_esq) e direita (l_dir)
	maior = -1000
	menor = 1000
	for i in range (0, n):
		if l[i].y < menor:
			menor = l[i].y
			menorp = l[i]
			menori = i
		if l[i].y > maior:
			maior = l[i].y
			maiorp = l[i]
			maiori = i

	l_esq = []
	l_dir = []

	l_esq.append(maiorp)
	suc = maiorp
	for i in range (0, menori-1):
		point = suc
		suc = find_sucessor(l, point)
		l_esq.append(suc)

	l_dir.append(menorp)
	suc = menorp
	for i in reversed (range (menori, n-1)):
		point = suc
		suc = find_sucessor(l, point)
		l_dir.append(suc)

	for i in range (0,n):
		lista_pre.append(find_predecessor(l,l_y[i]))
		lista_suc.append(find_sucessor(l,l_y[i]))

	# loop da triangulação
	for i in range (2,n):
		#l_y[i] é o ponto em questão
		point = l_y[i]

		for p in l_y:
			p.unhilight()

		point.hilight(color="blue")

		for p in S:
			p.hilight(color="yellow")


		# Caso a: point é adjacente ao primeiro ponto ta pilha e não adjacente ao último ponto da pilha
		if len(S) != 0 and (point == find_sucessor(l,S[-1]) or point == find_predecessor(l,S[-1])) and \
		(point != find_predecessor(l,S[0])) and (point != find_sucessor(l,S[0])):
			#print("Entrou no caso A")
			nS = len(S)
			# o if é utilizado pra verificar em qual curva poligonal o ponto está
			if nS <= 1 or S[-1].x <= S[-2].x:
				# enquanto tiver mais de um elemento em S
				# verifica se a diagonal a ser criada estará dentro do polígono
				while (nS > 1) and noCone(S[-2],S[-1],l):
					S.pop()
					nS = nS-1
					control.sleep()
					e = add_diagonal(point,S[-1])
					D.append(e)
					point.lineto(S[-1], color="blue")
					lista_pre[i] = S[-1]
			else:
				# enquanto tiver mais de um elemento em S
				# verifica se a diagonal a ser criada estará dentro do polígono
				while (nS > 1) and left(S[-1],S[-2],point):
					S.pop()
					nS = nS-1
					control.sleep()
					e = add_diagonal(point,S[-1])
					D.append(e)
					point.lineto(S[-1], color="blue")
					lista_pre[i] = S[-1]
			S.append(point)

		# Caso b: 	
		if len(S) != 0 and (point == find_sucessor(l,S[0]) or point == find_predecessor(l,S[0])) and \
		(point != find_predecessor(l,S[-1])) and (point != find_sucessor(l,S[-1])):
			#print("Entrou no caso B")
			aux = S[-1]
			nS = len(S)
			while nS > 1 and noCone(S[-1],point,l):
				control.sleep()
				# enquanto tiver mais de um elemento em S
				# verifica se a diagonal a ser criada estará dentro do polígono
				e = add_diagonal(point,S[-1])
				D.append(e)
				point.lineto(S[-1], color="blue")
				lista_pre[i] = S[-1]
				S.pop()
				nS = nS-1
			S.pop()
			S.append(aux)
			S.append(point)

		# Caso c:	
		if (point == find_sucessor(l,S[-1]) or point == find_predecessor(l,S[-1])) and \
		(point == find_sucessor(l,S[0]) or point == find_predecessor(l,S[0])):
			#print("Entrou no caso C")
			nS = len(S)
			S.pop()
			while nS > 2 and noCone(point,S[-1],l):
				# enquanto tiver mais de dois elemento em S
				# verifica se a diagonal a ser criada estará dentro do polígono
				nS = nS-1
				e = add_diagonal(point,S[-1])
				D.append(e)
				point.lineto(S[-1], color="blue")
				lista_pre[i] = S[-1]
				S.pop()

		control.sleep()

		# elimina o destaque do ponto e a linha
		point.unhilight()

	for p in l_y:
			p.unhilight()

	return D