# -*- coding: utf-8 -*-
from . import hertel_mehlhorn

# cada entrada deve ter:
#  [ 'nome-do-modulo', 'nome-da-funcao', 'nome do algoritmo' ]
children = ( 
	('hertel_mehlhorn', 'initialize_h', 'Hertel and Mehlhorn'),
)

#children = algorithms
#__all__ = [ 'graham', 'gift' ]
#__all__ = [a[0] for a in children]
__all__ = [ 'hertel_mehlhorn' ]