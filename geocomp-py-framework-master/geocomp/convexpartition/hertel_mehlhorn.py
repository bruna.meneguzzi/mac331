#!/usr/bin/env python
from geocomp.common.polygon import Polygon
from geocomp.common.segment import Segment
from geocomp.leepreparata.pointedge import MyPoint
from geocomp.leepreparata.pointedge import Edge
from geocomp.leepreparata.tree import BST
from geocomp.common import control
from geocomp.common.guiprim import *

from geocomp.leepreparata.support import *
from geocomp.leepreparata.leepreparata import *

import numpy as np
import time
import math

def initialize_h(l):
	# construção do polígono (pontos precisam estar na ordem correta e no sentido anti-horário)
	ret = Polygon(l)
	ret.plot()
	n = len(l)
	hert_mehl(l)

# algoritmo de hertel and mehlhorn
def hert_mehl(l):
	n = len(l)

	# D_Lee é a lista com os polígonos y-monótonos que a função monotone retorna
	# diag é a lista com as diagonais que o lee e preparata retorna
	D_Lee = []
	dupla = monotone(l)
	D_Lee = dupla[0]
	diag = dupla[1]

	list_d = diag
	# faz a triangulação em cada polígono y-monótono
	for p in D_Lee:
		D = monotone_triangulation(p)
		list_d.extend(D)

	# percorre cada diagonal e verifica se é essencial para
	# seus dois pontos extremos
	# se não for essencial, chama a função "remove_edge"
	# ilumina o ponto extremo a ser analisado com a cor "cyan"
	for d in list_d:
		idb = d.point.plot("cyan")
		control.sleep()
		d.point.unplot(idb)
		if not essencial(d):
			remove_edge(d)

	return

# verifica se a diagonal é essencial para o ponto analisado
def half_edge_essencial(diagonal):
	p = diagonal.point
	t = (diagonal.twin).point
	p.lineto(t,color="white")
	twin = diagonal.twin
	dpi = diagonal.prev.point
	tnf = (twin.next).twin.point
	if not left_on(dpi,p,tnf):
		p.remove_lineto(t)
		return True
	else: 
		p.remove_lineto(t)
		return False

# verifica se a diagonal é essencial para seus os dois extremos
def essencial(diagonal):
	return half_edge_essencial(diagonal) or half_edge_essencial(diagonal.twin)

# remove a diagonal, caso ela não seja essencial
def remove_edge(e):
	(e.point).lineto(e.twin.point,color="black")
	a1 = e.prev 
	a2 = (e.twin).next
	a3 = e.next
	a4 = (e.twin).prev
	a1.next = a2
	a2.prev = a1
	a3.prev = a4
	a4.next = a3
                               
if __name__ == '__main__':
    main()
